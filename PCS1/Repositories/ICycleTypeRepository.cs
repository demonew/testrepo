﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCS1.Repositories
{
   public interface ICycleTypeRepository<TEnt, in T> where TEnt : class
    {
        IEnumerable<TEnt> GetCycleType();
            
    }
}
