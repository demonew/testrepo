﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCS1.Repositories
{
   
    public interface IRepository<TEnt, in TPk,in Tran> where TEnt : class
    {
        IEnumerable<TEnt> Get();
        TEnt Get(TPk id);
        void Add(TEnt entity);
        void Remove(TEnt entity);
        TEnt CheckUser(Tran email);
        TEnt LoginUser(TEnt entity);
        bool ChangePassword(TEnt entity);      
        string SmtpUserName { get; }
        string SmtpPassword { get; }
        string SmtpPort { get; }
        string SenderEmail { get;  }
        string SmtpHost { get; }
    }
}
