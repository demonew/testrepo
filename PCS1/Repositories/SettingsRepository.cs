﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PCS1.Models;
namespace PCS1.Repositories
{
    public class SettingsRepository : ISettings<tbl_Settings>
    {
        dbpcsEntities context = new dbpcsEntities();
        public IEnumerable<tbl_Settings> GetSettings()
        {
            return context.tbl_Settings.ToList();
        }
    }
}