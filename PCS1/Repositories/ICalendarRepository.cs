﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCS1.Repositories
{
   
    public interface ICalendarRepository<TEnt, in T> where TEnt : class
    {
        IEnumerable<TEnt> GetPatients();
        TEnt GetPatients(T id);
        IEnumerable<TEnt> GetPatientsHistory(T id);
        void AddPatients(TEnt entity);
        void RemovePatients(TEnt entity);
        void EditPatients(TEnt entity,T id);
        void StopPatients(TEnt entity, T id);
       
    }
}
