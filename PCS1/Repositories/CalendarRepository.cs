﻿using PCS1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PCS1.Models;
using Microsoft.Practices.Unity;
using System.Data.Entity;

namespace PCS1.Repositories
{
    public class CalendarRepository : ICalendarRepository<tbl_Patient, long>
    {
        [Dependency]
        public dbpcsEntities context { get; set; }
        public void AddPatients(tbl_Patient entity)
        {
            entity.StartDate = DateTime.UtcNow.Date;
            context.tbl_Patient.Add(entity);
            context.SaveChanges();
        }
        public void EditPatients(tbl_Patient entity, long id)
        {
            var obj = context.tbl_Patient.Find(id);
            if (obj.CycleDate.Value.Date == entity.CycleDate.Value.Date && obj.CycleType == entity.CycleType && obj.BillingCycleStartDate.Value.Date == entity.BillingCycleStartDate.Value.Date && obj.BillingCycle == entity.BillingCycle && obj.PackagingType==entity.PackagingType)
            {
                obj.FirstName = entity.FirstName;
                obj.LastName = entity.LastName;
                obj.CycleDate = entity.CycleDate;
                obj.Comments = entity.Comments;
                obj.CycleType = entity.CycleType;
                obj.PackagingType = entity.PackagingType;
                obj.State = entity.State;
                obj.BillingCycleStartDate = entity.BillingCycleStartDate;
                obj.DOB = entity.DOB;
                context.SaveChanges();
            }
            else
            {
                DateTime cdt = obj.CycleDate.Value.Date;
                var patient = context.tbl_Patient.Where(p => p.SelfKey_Id == obj.SelfKey_Id && p.CycleDate >=cdt ).ToList();
                context.tbl_Patient.RemoveRange(patient);
                context.SaveChanges();
                DateTime lastdate= obj.CycleDate.Value.Date;
                var settings = context.tbl_Settings.ToList();
                foreach (var st in settings)
                {                   
                    if (st.key_id == "EndDate")
                        lastdate =Convert.ToDateTime(st.key_date.ToString());
                }
                entity.SelfKey_Id = obj.SelfKey_Id;
                entity.StartDate  = obj.StartDate;
                if (entity.CycleType == 1)
                {
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    obj.PackagingType = entity.PackagingType;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {
                        entity.CycleDate = dt;                      
                        if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                        context.tbl_Patient.Add(entity);
                        context.SaveChanges();
                        dt = dt.AddDays(7);
                        countbillingcycle++;
                        if (countbillingcycle == 5 && entity.BillingCycle == 1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }
                        else if (countbillingcycle == 13 && entity.BillingCycle == 2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                    }
                }
                else if (entity.CycleType == 2)
                {
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    obj.PackagingType = entity.PackagingType;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {
                        entity.CycleDate = dt;
                        if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                        context.tbl_Patient.Add(entity);
                        context.SaveChanges();
                        dt = dt.AddDays(14);
                        countbillingcycle++;
                        if (countbillingcycle == 3 && entity.BillingCycle == 1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }
                        else if (countbillingcycle == 7 && entity.BillingCycle == 2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                    }
                }
                else if (entity.CycleType == 3)
                {
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    obj.PackagingType = entity.PackagingType;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {
                        entity.CycleDate = dt;
                        if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                        context.tbl_Patient.Add(entity);
                        context.SaveChanges();
                        dt = dt.AddDays(28);
                       countbillingcycle++;
                        if (countbillingcycle == 4 && entity.BillingCycle == 2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                        else if (entity.BillingCycle == 1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }                    
                    }
                }
                else if (entity.CycleType == 4)
                {
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    obj.PackagingType = entity.PackagingType;
                    int countcycle = 1, count = 1;
                    while (dt < lastdate)
                    {
                        if (entity.BillingCycle == 1)
                        {
                            if (countcycle == 1)
                                entity.CycleDate = dt;
                            else
                                entity.CycleDate = null;
                            entity.BillingCycleStartDate = billdt;
                            context.tbl_Patient.Add(entity);
                            context.SaveChanges();
                            count++;
                            if (count == 4)
                            {
                                dt = dt.AddDays(84);
                                countcycle = 1;
                                count = 1;
                            }
                            else
                                countcycle = 2;
                            billdt = billdt.AddDays(28);
                        }
                        else if (entity.BillingCycle == 2)
                        {
                            entity.CycleDate = dt;
                            entity.BillingCycleStartDate = billdt;
                            context.tbl_Patient.Add(entity);
                            context.SaveChanges();
                            dt = dt.AddDays(84);
                            billdt = billdt.AddDays(84);
                        }
                    }
                }
                else if (entity.CycleType == 5)
                {
                    obj.FirstName = entity.FirstName;
                    obj.LastName = entity.LastName;
                    obj.CycleDate = entity.CycleDate;
                    obj.Comments = entity.Comments;
                    obj.CycleType = entity.CycleType;
                    obj.PackagingType = entity.PackagingType;
                    obj.State = entity.State;
                    obj.BillingCycleStartDate = entity.BillingCycleStartDate;
                    obj.DOB = entity.DOB;
                    context.tbl_Patient.Add(obj);
                    context.SaveChanges();
                }
            }
        }
        public void StopPatients(tbl_Patient entity, long id)
        {
            var obj = context.tbl_Patient.Find(id);          
            DateTime cdt = obj.CycleDate.Value.Date;
            var patient = context.tbl_Patient.Where(p => p.SelfKey_Id == obj.SelfKey_Id && p.CycleDate > cdt).ToList();
            context.tbl_Patient.RemoveRange(patient);
            context.SaveChanges();              
        }
        public IEnumerable<tbl_Patient> GetPatients()
        {
            context.Configuration.ProxyCreationEnabled = false;
            return context.tbl_Patient.ToList();
        }

        public tbl_Patient GetPatients(long id)
        {
            return context.tbl_Patient.Find(id);
        }
        public IEnumerable<tbl_Patient> GetPatientsHistory(long id)
        {
            return context.tbl_Patient.Where(p=>p.SelfKey_Id==id).ToList();
        }
        public void RemovePatients(tbl_Patient entity)
        {
            var obj = context.tbl_Patient.Find(entity.Patient_Id);
            var patient = context.tbl_Patient.Where(p => p.SelfKey_Id == obj.SelfKey_Id).ToList();
            context.tbl_Patient.RemoveRange(patient);
            context.SaveChanges();
        }
      
    }
}