﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using PCS1.Models;

namespace PCS1.Repositories
{
    public class CycleTypeRepository : ICycleTypeRepository<tbl_CycleType, int>
    {
        [Dependency]
        dbpcsEntities context { get; set; }
    public IEnumerable<tbl_CycleType> GetCycleType()
        {
           return context.tbl_CycleType.ToList();
        }
    }
}