﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using PCS1.Models;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace PCS1.Repositories
{
    // The User Repository Class.This is used to
    //Isolate the EntityFramework based Data Access Layer from
    //the MVC Controller class
    public class UserRepository : IRepository<tbl_User, int, string>
    {
        Encrypt_Decrypt ed = new Encrypt_Decrypt();
        [Dependency]
        public dbpcsEntities context { get; set; }

        public string SmtpUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPUsername"];
            }
        }
        public string SmtpPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPPassword"];
            }
        }
        public string SmtpPort
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPServerPort"];
            }
        }
        public string SenderEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["SenderEmail"];
            }
        }
        public string SmtpHost
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpHost"];
            }
        }
        public void Add(tbl_User entity)
        {
            entity.Password = ed.Encrypt(entity.Password);
            entity.ConfirmPassword = ed.Encrypt(entity.ConfirmPassword);
            context.tbl_User.Add(entity);
            context.SaveChanges();
        }

        public IEnumerable<tbl_User> Get()
        {
            return context.tbl_User.ToList();
        }

        public tbl_User Get(int id)
        {
            return context.tbl_User.Find(id);
        }

        public void Remove(tbl_User entity)
        {
            var obj = context.tbl_User.Find(entity.User_Id);
            context.tbl_User.Remove(obj);
            context.SaveChanges();
        }
        public tbl_User CheckUser(string email)
        {
            var user = context.tbl_User.Where(p => p.Email == email).FirstOrDefault();
            return user;
        }
        public tbl_User LoginUser(tbl_User entity)
        {
            entity.Password = ed.Encrypt(entity.Password);
            var user = context.tbl_User.Where(p => p.Username.ToUpper() == entity.Username.ToUpper() && p.Password == entity.Password).FirstOrDefault();
            return user;
        }
        public bool ChangePassword(tbl_User entity)
        {
            string enc_pwd = ed.Encrypt(entity.Password);
            var user = context.tbl_User.Where(p => p.Email == entity.Email && p.Password == enc_pwd).FirstOrDefault();
            if (user != null)
            {
                user.Password = ed.Encrypt(entity.NewPassword);
                user.ConfirmPassword = ed.Encrypt(entity.NewPassword);
                context.SaveChanges();
                return true;
            }
            else
                return false;
        }
       
    
    }
}