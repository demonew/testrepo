﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCS1.Repositories
{
  public interface ISettings<TEnt> where TEnt:class
    {
        IEnumerable<TEnt> GetSettings();
    }
}
