﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using PCS1.Models;
using PCS1.ViewModel;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System;

namespace PCS1.Controllers
{
    [RouteArea("Admin")]
    [Route("{action=Login}")]
    [Route("{action}/{id=value}")]
    public class AdminController : Controller
    {
        dbpcsEntities context = new dbpcsEntities();
        Encrypt_Decrypt ed = new Encrypt_Decrypt();

        public string SmtpUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPUsername"];
            }
        }
        public string SmtpPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPPassword"];
            }
        }
        public string SmtpPort
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPServerPort"];
            }
        }
        public string SenderEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["SenderEmail"];
            }
        }
        public string SmtpHost
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpHost"];
            }
        }

        // GET: Admin/myprofile
        public ActionResult MyProfile()
        {
            string email = "";
            if (Session["Admin"] != null)
                email = Session["Admin"].ToString();         
            var admin = context.tbl_Admin.Where(p => p.Email == email).FirstOrDefault();
            return View(admin);
        }
        // GET: Admin/editprofile
        public ActionResult EditProfile(int id)
        {
            if (Session["Admin"] != null)
            {
                var admin = context.tbl_Admin.Where(p => p.Admin_Id==id).FirstOrDefault();
            admin.Password = ed.Decrypt(admin.Password);
                
            return View(admin);
            }
            else
                return RedirectToAction("Login", "Pcs", new { area = "" });
        }
        // POST: Admin/editprofile
        [HttpPost]
        public ActionResult EditProfile(int? Admin_Id,tbl_Admin Admin,HttpPostedFileBase Photo)
        {
            try
            {                
                var admin = context.tbl_Admin.Where(p => p.Admin_Id == Admin_Id).FirstOrDefault();
                admin.FirstName = Admin.FirstName;
                admin.LastName = Admin.LastName;
                admin.Password =ed.Encrypt(Admin.Password);               
                string pic = "";
                admin.Photo = pic;
                if (Photo != null)
                {
                    pic = System.IO.Path.GetFileName(Photo.FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/images/"), pic);
                    Photo.SaveAs(path);
                    admin.Photo = pic;
                }
                context.SaveChanges();
                return RedirectToAction("MyProfile",admin);
            }
            catch
            {
                return View();
            }
        }
        // GET: admin/employeelist
        public ActionResult Employee()
        {
            if (Session["Admin"] != null)
            {
                var users = context.tbl_User.ToList();
                return View(users);
            }
            else
                return RedirectToAction("Login", "Pcs", new { area = "" });
        }

        // POST: admin/employeelist
        [HttpPost]
        public ActionResult Employee(ChangePassword User)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbl_User usr = new tbl_User();
                    usr.Email = Session["Admin"].ToString();
                    usr.Password = User.Password;
                    usr.NewPassword = User.NewPassword;
                    User.Email = usr.Email;
                    bool chk = true;
                    string enc_pwd = ed.Encrypt(usr.Password);
                    var user = context.tbl_User.Where(p => p.Email == usr.Email && p.Password == enc_pwd).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = usr.NewPassword;
                        user.ConfirmPassword = usr.NewPassword;
                        context.SaveChanges();
                    }
                    else
                        chk = false;
                    if (chk == true)
                    {
                        TempData["CPMessage"] = "Password Changed Successfully";
                        return View(User);
                    }
                    else
                    {
                        TempData["CPMessage"] = "Something Went Wrong";
                        return View(User);
                    }
                }
                else
                    return View(User);
            }

            catch
            {
                return View();
            }
        }
        // GET: admin/ChangePassword
        public ActionResult ChangePassword(int? id)
        {
            if (Session["Admin"] != null)
            {
                var usr = context.tbl_User.Where(p => p.User_Id == id).FirstOrDefault();
                var user = new ChangePassword();
                user.Email = usr.Email;
                return View(user);
            }
            else
                return RedirectToAction("Login", "Pcs", new { area = "" });
        }

        // POST: admin/ChangePassword
        [HttpPost]
        public ActionResult ChangePassword(string Email, ChangePassword User)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbl_User usr = new tbl_User();
                    usr.Email = Email;
                    usr.Password = User.Password;
                    usr.NewPassword = User.NewPassword;
                    User.Email = usr.Email;
                    bool chk = true;
                    string enc_pwd = ed.Encrypt(usr.Password);
                    var user = context.tbl_User.Where(p => p.Email == usr.Email && p.Password == enc_pwd).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password =ed.Encrypt(usr.NewPassword);
                        user.ConfirmPassword = ed.Encrypt(usr.NewPassword);
                        context.SaveChanges();
                    }
                    else
                        chk = false;
                    if (chk == true)
                    {
                        TempData["CPMessage"] = "Password Changed Successfully";
                        return Json(new { success = true });
                    }
                    else
                    {
                        TempData["CPMessage"] = "Something Went Wrong";
                        return View(User);
                    }
                }
                else
                    return View(User);
            }

            catch
            {
                return View();
            }
        }

        // GET: admin/ForgetPassword
        public ActionResult ForgetPassword()
        {
            return View();
        }
        // POST: admin/ForgetPassword
        [HttpPost]
        public ActionResult ForgetPassword(ForgetPassword fp)
        {
            //try
            //{
                if (ModelState.IsValid)
                {
                    if ((FP(fp.Email)) == true)
                    {
                        TempData["FPMessage"] = "Password has been successfully Sent on your email.Please check";
                    }
                    else
                    {
                        TempData["FPMessage"] = "Something Went Wrong";
                    }
                }
                return View();
            //}
            //catch
            //{
            //    return View();
            //}
        }
        public bool FP(string Email)
        {
            string password = context.tbl_Admin.Where(p => p.Email== Email).Select(p => p.Password).FirstOrDefault();
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress(SenderEmail);
            mm.To.Add(Email);
            mm.Subject = "Forget Password Email";
            string s = "<b>Hi...</b><br/>";
            s += "<b>Greetings!!</b><br/>";
            s += "<br/>";
            s += "<br/>";
            s += "<b>Email Address : </b>" + Email + "<br/>";
            s += "<b>Password : </b>" + ed.Decrypt(password) + "<br/>";
            s += "<br/>";
            s += "<br/>";
            s += "<b> Best Regards,</b><br/>";
            s += "<b>MedMinder</b><br/>";
            mm.Body = s;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = SmtpHost;
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = SmtpUserName;
            NetworkCred.Password = SmtpPassword;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = Convert.ToInt32(SmtpPort);
            smtp.Send(mm);
            return true;
        }
        public ActionResult settings()
        {
            if (Session["Admin"] != null)
            {
                var q = context.tbl_Settings.ToList();
            return View(q);
            }
            else
                return RedirectToAction("Login", "Pcs", new { area = "" });
        }
        public ActionResult Editsettings(int id)
        {
            if (Session["Admin"] != null)
            {
                var q = context.tbl_Settings.Find(id);
            return View(q);
            }
            else
                return RedirectToAction("Login", "Pcs", new { area = "" });
        }
        [HttpPost]
        public ActionResult Editsettings(int id,tbl_Settings tbl_settings)
        {
            tbl_Settings settings = context.tbl_Settings.Find(id);
            settings.slug = tbl_settings.slug;
            settings.value = tbl_settings.value;
            settings.key_date = tbl_settings.key_date;
            context.SaveChanges();
            return Json(new { success = true });
        }
    }
}
