﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PCS1.Models;
using PCS1.Repositories;
using PCS1.ViewModel;
using PagedList;
using System.Web.Script.Serialization;
using System.Web;

namespace PCS1.Controllers
{
    public class PcsController : Controller
    {
        Encrypt_Decrypt ed = new Encrypt_Decrypt();
        dbpcsEntities db = new dbpcsEntities();
        //Property of the type IRepository <TEnt, in TPk>
        private IRepository<tbl_User, int,string > _repository;
        private ICalendarRepository<tbl_Patient, long> _calrepository;
        private ISettings<tbl_Settings> _settingsrepository;
        static string ln,st;
        static int? packaging=0;
        static DateTime? fd, td;
        public PcsController(IRepository<tbl_User, int,string> repo, ICalendarRepository<tbl_Patient, long> prepo,ISettings<tbl_Settings> srepo)
        {
            _repository = repo;
            _calrepository = prepo;
            _settingsrepository = srepo;
        }
        // GET: Pcs/UserLogin
        public ActionResult Login()
        {
            return View();
        }
        // POST: Pcs/UserLogin
        [HttpPost]
        public ActionResult Login(UserLogin User)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tbl_User usr = new tbl_User();
                    usr.Username = User.Username;
                    usr.Password = User.Password;
                    var user = _repository.LoginUser(usr);
                    tbl_Admin Admin = new tbl_Admin();
                    Admin.UserName = User.Username;
                    Admin.Password = User.Password;
                    var password = ed.Encrypt(Admin.Password);
                    var admin = db.tbl_Admin.Where(p => p.UserName.ToUpper() == Admin.UserName.ToUpper() && p.Password == password).FirstOrDefault();                    
                    if (user != null)
                    {
                        Session["User"] = user.Email;
                        HttpCookie UserDetailsCookie = new HttpCookie("LoggedInUser");
                        UserDetailsCookie.Name = "LoggedInUser";
                        UserDetailsCookie.Expires = DateTime.MaxValue;
                        UserDetailsCookie.Values.Add("UserName", user.Email);                       
                        Response.Cookies.Set(UserDetailsCookie);
                        Session["User"] = user.Email;
                        return RedirectToAction("Calendar");
                    }
                    else if(admin != null)
                    {
                        Session["Admin"] = admin.Email;
                        return RedirectToAction("MyProfile","Admin");
                    }
                    else
                    {
                        TempData["LoginMessage"] = "Wrong Username/Password";
                        return View(User);
                    }
                }
                else
                    return View();
        }
            catch
            {
                return View();
    }
}
        // GET: Pcs/UserRegister
        public ActionResult ChangePassword()
        {
            try
            {
                if (Session["User"] != null)
                {
                    var user = new ChangePassword();
                    user.Email = Session["User"].ToString();
                    return View(user);
                }
                else
                    return RedirectToAction("Login");
            }
            catch
            {
                return View();
            }
}
        // POST: Pcs/UserRegister
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword User)
        {
            try
            {
                if (Session["User"] != null)
                {
                    if (ModelState.IsValid)
                    {
                        tbl_User usr = new Models.tbl_User();
                        usr.Email = Session["User"].ToString();
                        usr.Password = User.Password;
                        usr.NewPassword = User.NewPassword;
                        User.Email = usr.Email;
                        bool chk = _repository.ChangePassword(usr);
                        if (chk == true)
                        {
                            TempData["CPMessage"] = "Password Changed Successfully";
                            return View(User);
                        }
                        else
                        {
                            TempData["CPMessage"] = "Something Went Wrong";
                            return View(User);
                        }
                    }
                    else
                        return View(User);
                }
                else
                    return RedirectToAction("Login");               
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                          validationErrors.Entry.Entity.ToString(),
                       validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
        // GET: Pcs Calendar
        public ActionResult Calendar()
        {
            //if (Session["User"] != null)
            //{
            HttpCookie UserDetailsCookie = Request.Cookies["LoggedInUser"];
            if (UserDetailsCookie != null)
            {
                Session["User"] = UserDetailsCookie["UserName"];
            }
            return View();
            //}
            //else
            //    return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult Calendar(string command, tbl_Patient entity,DateTime? date3)
        {
            try
            {
                //if (Session["User"] != null)
                //{
                    if (command == "Update")
                        _calrepository.EditPatients(entity, entity.Patient_Id);
                    else if (command == "Delete")
                        _calrepository.RemovePatients(entity);
                    else if (command == "Stop")
                        _calrepository.StopPatients(entity, entity.Patient_Id);
                    else if (command == "PrintShipping")
                    {
                    Session["selecteddate"] = date3.Value.ToShortDateString();
                        List<tbl_Patient> patientList = null;
                        patientList = _calrepository.GetPatients().Where(p=>p.CycleDate==date3.Value).OrderBy(p => p.LastName).ToList();
                        var builder = new PdfBuilder(patientList, Server.MapPath("/PCS/Views/Pcs/ConvertToPdfSelected.cshtml"), Server.MapPath("/PCS/Content/Site.css"));
                        return builder.GetPdf1();
                }
                else if (command == "PrintBilling")
                {
                    Session["selecteddate"] = date3.Value.ToShortDateString();
                    List<tbl_Patient> patientList = null;
                    patientList = _calrepository.GetPatients().Where(p => p.BillingCycleStartDate == date3.Value).OrderBy(p => p.LastName).ToList();
                    var builder = new PdfBuilder(patientList, Server.MapPath("/PCS/Views/Pcs/ConvertToPdfSelectedBilling.cshtml"), Server.MapPath("/PCS/Content/Site.css"));
                    return builder.GetPdf1();
                }
                return View();
                //}
                //else
                //{
                //    return RedirectToAction("Login");
                //}
        }
            catch(Exception ex)
            {
                return View();
    }
}
        public long getselfid()
        {
            long id = 0;
            var getlist = _calrepository.GetPatients();
            if (getlist.Count() == 0)
                id = 1;
            else
            {
                long maxid = getlist.Max(p => p.SelfKey_Id).Value;
                id = ++maxid;
            }
            return id;
        }
        // GET: Pcs/getPatients
        public ActionResult getPatients()
        {
            try
            {
                List<PatientDetails> PatientList = new List<PatientDetails>();
                var patientList = _calrepository.GetPatients().Where(p=>p.BillingCycleStartDate!=null).ToList();
                bool a = true;
                ranges rg = new ranges();
                List<ranges> lstrng = new List<ranges>();                      
                foreach (var q in patientList)
                {                   
                    if (a == true)
                    {
                        var settings = _settingsrepository.GetSettings();
                        foreach (var st in settings)
                        {
                            if (st.key_id == "StartDate")
                                rg.start = st.key_date.ToString();
                            if (st.key_id == "EndDate")
                                rg.end = st.key_date.ToString();
                        }
                        lstrng.Add(rg);
                        a = false;
                    }                
                    PatientDetails pd = new PatientDetails();
                    pd.id = Convert.ToString(q.Patient_Id);
                    pd.title = q.FirstName + " " + q.LastName;
                    pd.start =(DateTime)(q.BillingCycleStartDate);
                    pd.end = (DateTime)(q.BillingCycleStartDate);
                    pd.color = "#DC143C";
                    pd.ranges = lstrng;
                    PatientList.Add(pd);               
                }
                var patientListshipping = _calrepository.GetPatients().Where(p=> p.CycleDate != null);
                foreach (var q in patientListshipping)
                {
                    if (a == true)
                    {
                        var settings = _settingsrepository.GetSettings();
                        foreach (var st in settings)
                        {
                            if (st.key_id == "StartDate")
                                rg.start = st.key_date.ToString();
                            if (st.key_id == "EndDate")
                                rg.end = st.key_date.ToString();
                        }
                        lstrng.Add(rg);
                        a = false;
                    }
                    PatientDetails pd = new PatientDetails();
                    pd.id = Convert.ToString(q.Patient_Id);
                    pd.title = q.FirstName + " " + q.LastName;
                    pd.start = q.CycleDate.Value.Date;
                    pd.end = q.CycleDate.Value.Date;
                    if(q.CycleType==5)
                    pd.color = "#2E8B57";
                    else
                    pd.color = "#4682B4";
                    pd.ranges = lstrng;
                    PatientList.Add(pd);
                }
                var rows = PatientList.ToArray();
                var jsonResult= Json(rows, JsonRequestBehavior.AllowGet);               
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
}
        // GET: Pcs/getPatients/filters
        public ActionResult getFilteredPatients(string lastname)
        {
            try
            {
                List<PatientDetails> PatientList = new List<PatientDetails>();
                var pppp = _calrepository.GetPatients().ToList();
                var patientList = _calrepository.GetPatients().Where(p => p.LastName.ToUpper().Contains(lastname.ToUpper()) && p.BillingCycleStartDate!=null).OrderBy(p=>p.LastName).ToList();
                bool a = true;
                ranges rg = new ranges();
                List<ranges> lstrng = new List<ranges>();
                foreach (var q in patientList)
                {
                    if (a == true)
                    {
                        var settings = _settingsrepository.GetSettings();
                        foreach (var st in settings)
                        {
                            if (st.key_id == "StartDate")
                                rg.start = st.key_date.ToString();
                            if (st.key_id == "EndDate")
                                rg.end = st.key_date.ToString();
                        }
                        lstrng.Add(rg);
                        a = false;
                    }
                    PatientDetails pd = new PatientDetails();
                    pd.id = Convert.ToString(q.Patient_Id);
                    pd.title = q.FirstName + " " + q.LastName;
                    pd.start = (DateTime)(q.BillingCycleStartDate);
                    pd.end = (DateTime)(q.BillingCycleStartDate);
                    pd.color = "#DC143C";
                    pd.ranges = lstrng;
                    PatientList.Add(pd);
                }
                var patientListshipping = _calrepository.GetPatients().Where(p => p.LastName.ToUpper().Contains(lastname.ToUpper()) && p.CycleDate!=null).OrderBy(p => p.LastName).ToList(); 
                foreach (var q in patientListshipping)
                {
                    if (a == true)
                    {
                        var settings = _settingsrepository.GetSettings();
                        foreach (var st in settings)
                        {
                            if (st.key_id == "StartDate")
                                rg.start = st.key_date.ToString();
                            if (st.key_id == "EndDate")
                                rg.end = st.key_date.ToString();
                        }
                        lstrng.Add(rg);
                        a = false;
                    }
                    PatientDetails pd = new PatientDetails();
                    pd.id = Convert.ToString(q.Patient_Id);
                    pd.title = q.FirstName + " " + q.LastName;
                    pd.start = q.CycleDate.Value.Date;
                    pd.end = q.CycleDate.Value.Date;
                    pd.color = "#4682B4";
                    pd.ranges = lstrng;
                    PatientList.Add(pd);
                }
                var rows = PatientList.ToArray();
                var jsonResult = Json(rows, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch
            {                
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        // GET: Pcs/AddPatients
        public ActionResult addPatients()
        {            
                return View();           
        }
        // POST: Pcs/AddPatients
        [HttpPost]
        public ActionResult addPatients(tbl_Patient entity)
        {
            try
            {             
                PatientDetails pd = new PatientDetails();
                ranges rg = new ranges();                
                List<ranges> lstrng = new List<ranges>();
                var settings = _settingsrepository.GetSettings();
                foreach (var st in settings)
                {
                    if (st.key_id == "StartDate")
                        rg.start = st.key_date.ToString();
                    if (st.key_id == "EndDate")
                        rg.end = st.key_date.ToString();
                }
                lstrng.Add(rg);
                DateTime lastdate =Convert.ToDateTime(rg.end);
                if (entity.CycleType == 1)
                {
                    long selfid= getselfid();
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {                      
                        entity.CycleDate = dt;
                        entity.SelfKey_Id = selfid;
                        if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                            _calrepository.AddPatients(entity);
                        dt = dt.AddDays(7);
                        countbillingcycle++;
                        if (countbillingcycle == 5 && entity.BillingCycle==1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }
                        else if(countbillingcycle == 13 && entity.BillingCycle == 2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                    }
                }
                else if (entity.CycleType == 2)
                {
                    long selfid = getselfid();
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {
                        entity.CycleDate = dt;
                        entity.SelfKey_Id = selfid;
                        if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                        _calrepository.AddPatients(entity);
                        dt =  dt.AddDays(14);
                        countbillingcycle++;
                        if (countbillingcycle == 3 && entity.BillingCycle == 1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }
                        else if (countbillingcycle == 7 && entity.BillingCycle == 2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                    }
                }
                else if (entity.CycleType == 3)
                {
                    long selfid = getselfid();
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    int countbillingcycle = 1;
                    while (dt < lastdate)
                    {
                        entity.CycleDate = dt;
                        entity.SelfKey_Id = selfid;                 
                          if (countbillingcycle == 1)
                            entity.BillingCycleStartDate = billdt;
                        else
                            entity.BillingCycleStartDate = null;
                        _calrepository.AddPatients(entity);
                        dt =  dt.AddDays(28);
                        countbillingcycle++;
                        if (countbillingcycle == 4 && entity.BillingCycle ==2)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(84);
                        }
                        else if(entity.BillingCycle == 1)
                        {
                            countbillingcycle = 1;
                            billdt = billdt.AddDays(28);
                        }
                    }
                }
                else if (entity.CycleType == 4)
                {
                    long selfid = getselfid();
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    int countcycle=1,count=1;
                    while (dt < lastdate)
                    {
                        if (entity.BillingCycle == 1)
                        {
                                if (countcycle == 1)
                                    entity.CycleDate = dt;
                                else
                                    entity.CycleDate = null;                                                                              
                            entity.SelfKey_Id = selfid;
                            entity.BillingCycleStartDate = billdt;
                            _calrepository.AddPatients(entity);
                            count++;
                            if (count == 4)
                            {
                                dt = dt.AddDays(84);
                                countcycle = 1;
                                count = 1;
                            }                                                                                     
                            else                            
                                countcycle = 2;
                                billdt = billdt.AddDays(28);                            
                        }
                        else if(entity.BillingCycle==2)
                        {
                            entity.SelfKey_Id = selfid;
                            entity.CycleDate = dt;                           
                            entity.BillingCycleStartDate = billdt;
                            _calrepository.AddPatients(entity);                                                                                  
                            dt = dt.AddDays(84);
                            billdt = billdt.AddDays(84);                           
                        }                    
                    }
                }
                else if (entity.CycleType == 5)
                {
                    long selfid = getselfid();
                    entity.SelfKey_Id = selfid;
                    DateTime dt = entity.CycleDate.Value.Date;
                    DateTime billdt = entity.BillingCycleStartDate.Value.Date;
                    _calrepository.AddPatients(entity);
                }
                return Json(new { success = true });               
            }
            catch
            {
                return View();
            }
        }
        //GET :getPatientDetails
        public ActionResult getPatientDetails(int id)
        {
            try
            { 
            var obj = _calrepository.GetPatients(id);
            return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetList(int? page)
        {
            try
            {
                //if (Session["User"] != null)
                 HttpCookie UserDetailsCookie = Request.Cookies["LoggedInUser"];
                if (UserDetailsCookie != null)
                {
                    Session["User"] = UserDetailsCookie["UserName"];
                }
                Session.Remove("fromdate");
                Session.Remove("todate");
                ln = "";
                st = "State";
                packaging = 0;
                fd = null;
                td = null;
                int pageSize = 8;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                List<tbl_Patient> patientList  = null;
                IPagedList<tbl_Patient> patientListPaged = null;
                patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p=>p.LastName).ToList();              
                patientListPaged = patientList.ToPagedList(pageIndex, pageSize);
                return View(patientListPaged);
            //}
            //else
            //    return RedirectToAction("Login");
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        public ActionResult GetList(string command,int? page,string lastname,DateTime? fromdate ,DateTime? todate,int? PackagingType,string State)
        {
            try
            {
                int pageSize = 8;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                List<tbl_Patient> patientList = null;
                IPagedList<tbl_Patient> patientListPaged = null;
                if (command == "search")
                {
                    ViewData["lastname"] = lastname;                    
                    ln = lastname;
                    st = State;
                    packaging = PackagingType;
                    if (lastname != null && lastname != "")
                        patientList = _calrepository.GetPatients().Where(p => p.LastName.ToUpper().Contains(lastname.ToUpper())).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    else
                        patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if (fromdate != null && todate != null)
                    {
                        ViewData["fromdate"] = fromdate.Value.ToShortDateString();
                        ViewData["todate"] = todate.Value.ToShortDateString();
                        fd = fromdate;
                        td = todate;
                        patientList = patientList.Where(p => p.CycleDate >= fromdate.Value.Date && p.CycleDate <= todate.Value.Date).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    }
                    if(PackagingType!=0)
                    patientList = patientList.Where(p => p.PackagingType==PackagingType).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if(State != "State")
                    patientList = patientList.Where(p => p.State == State).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if ((lastname == null || lastname == "") && fromdate == null && todate == null &&PackagingType==0 && State== "State")
                        patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();

                }
                patientListPaged = patientList.ToPagedList(pageIndex, pageSize);
                return View(patientListPaged);
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult GetHistory(long id, int? page)
        {
            //if (Session["User"] != null)
            //{
            HttpCookie UserDetailsCookie = Request.Cookies["LoggedInUser"];
            if (UserDetailsCookie != null)
            {
                Session["User"] = UserDetailsCookie["UserName"];
            }
            int pageSize = 8;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                IPagedList<tbl_Patient> model =  _calrepository.GetPatientsHistory(id).Where(p=>p.CycleDate!=null).ToPagedList(pageIndex, pageSize);
                return View(model);
            //}
            //else
            //    return RedirectToAction("Login");
        }
        public ActionResult DownloadPatientDetails()
        {
            try
            {
                Session.Remove("fromdate");
                Session.Remove("todate");
               // Patients patientsdet = new Patients();
                List<tbl_Patient> patientList = null;
                patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
            Session["fromdate"] = "All";
            Session["todate"] = "Dates";
            if (ln != null && ln != "")
                {
                    patientList = patientList.Where(p => p.LastName.ToUpper() == ln.ToUpper()).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                }
                if (fd != null && td != null)
                {
                    Session["fromdate"] = fd.Value.ToShortDateString()+"-";
                    Session["todate"] = td.Value.ToShortDateString();
                    patientList = patientList.Where(p => p.CycleDate >= fd.Value.Date && p.CycleDate <= td.Value.Date).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                }
                if (packaging != 0)
                    patientList = patientList.Where(p => p.PackagingType == packaging).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                if (st != "State")
                    patientList = patientList.Where(p => p.State == st).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                patientList = patientList.Where(p => p.CycleDate != null).OrderBy(p => p.LastName).ToList();
                var builder = new PdfBuilder(patientList, Server.MapPath("/PCS/Views/Pcs/ConvertToPdf.cshtml"), Server.MapPath("/PCS/Content/Site.css"));
                return builder.GetPdf1();
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult ConvertToPdf()
        {
            IEnumerable<tbl_Patient> data = _calrepository.GetPatients();           
            return View(data);
        }

        public ActionResult GetBillingList(int? page)
        {
            try
            {
                //if (Session["User"] != null)
                //{
                HttpCookie UserDetailsCookie = Request.Cookies["LoggedInUser"];
                if (UserDetailsCookie != null)
                {
                    Session["User"] = UserDetailsCookie["UserName"];
                }
                Session.Remove("fromdate");
                    Session.Remove("todate");
                    ln = "";
                    st = "State";
                packaging = 0;
                    fd = null;
                    td = null;
                    int pageSize = 8;
                    int pageIndex = 1;
                    pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                    List<tbl_Patient> patientList = null;
                    IPagedList<tbl_Patient> patientListPaged = null;
                    patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    patientListPaged = patientList.ToPagedList(pageIndex, pageSize);
                    return View(patientListPaged);
                //}
                //else
                //    return RedirectToAction("Login");
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        public ActionResult GetBillingList(string command, int? page, string lastname, DateTime? fromdate, DateTime? todate, int? PackagingType, string State)
        {
            try
            {
                int pageSize = 8;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                List<tbl_Patient> patientList = null;
                IPagedList<tbl_Patient> patientListPaged = null;
                if (command == "search")
                {
                    ViewData["lastname"] = lastname;
                    ln = lastname;
                    st = State;
                    packaging = PackagingType;
                    if (lastname != null && lastname != "")
                        patientList = _calrepository.GetPatients().Where(p => p.LastName.ToUpper().Contains(lastname.ToUpper())).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    else
                        patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if (fromdate != null && todate != null)
                    {
                        ViewData["fromdate"] = fromdate.Value.ToShortDateString();
                        ViewData["todate"] = todate.Value.ToShortDateString();
                        fd = fromdate;
                        td = todate;
                        patientList = patientList.Where(p => p.BillingCycleStartDate >= fromdate.Value.Date && p.BillingCycleStartDate <= todate.Value.Date).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    }
                    if (PackagingType != 0)
                        patientList = patientList.Where(p => p.PackagingType==PackagingType).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if (State != "State")
                        patientList = patientList.Where(p => p.State == State).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                    if ((lastname == null || lastname == "") && fromdate == null && todate == null && PackagingType == 0 && State == "State")
                        patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                }
                patientListPaged = patientList.ToPagedList(pageIndex, pageSize);
                return View(patientListPaged);
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult GetBillingHistory(long id, int? page)
        {
            //if (Session["User"] != null)
            //{
            HttpCookie UserDetailsCookie = Request.Cookies["LoggedInUser"];
            if (UserDetailsCookie != null)
            {
                Session["User"] = UserDetailsCookie["UserName"];
            }
            int pageSize = 8;
                int pageIndex = 1;
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
                IPagedList<tbl_Patient> model = _calrepository.GetPatientsHistory(id).Where(p => p.BillingCycleStartDate != null).ToPagedList(pageIndex, pageSize);
                return View(model);
            //}
            //else
            //    return RedirectToAction("Login");
        }

        public ActionResult DownloadPatientDetailsBilling()
        {
            try
            {
                Session.Remove("fromdate");
                Session.Remove("todate");
                // Patients patientsdet = new Patients();
                List<tbl_Patient> patientList = null;
                patientList = _calrepository.GetPatients().GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p=>p.LastName).ToList();
                Session["fromdate"] = "All";
                Session["todate"] = "Dates";
                if (ln != null && ln != "")
                {
                    patientList = patientList.Where(p => p.LastName.ToUpper() == ln.ToUpper()).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                }
                if (fd != null && td != null)
                {
                    Session["fromdate"] = fd.Value.ToShortDateString() + "-";
                    Session["todate"] = td.Value.ToShortDateString();
                    patientList = patientList.Where(p => p.BillingCycleStartDate >= fd.Value.Date && p.BillingCycleStartDate <= td.Value.Date).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                }
                if (packaging != 0)
                    patientList = patientList.Where(p => p.PackagingType == packaging).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                if (st != "State")
                    patientList = patientList.Where(p => p.State == st).GroupBy(x => x.SelfKey_Id).Select(x => x.First()).OrderBy(p => p.LastName).ToList();
                patientList = patientList.Where(p => p.BillingCycleStartDate != null).ToList();
                 var builder = new PdfBuilder(patientList, Server.MapPath("/PCS/Views/Pcs/ConvertToPdfBilling.cshtml"), Server.MapPath("/PCS/Content/Site.css"));
                return builder.GetPdf1();
            }
            catch(Exception ex)
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult AutocompleteSuggestions(string LastName)
        {
            var autocompleteList = _calrepository.GetPatients().Where(p => p.LastName.ToLower().Contains(LastName.ToLower())).GroupBy(x => x.SelfKey_Id).Select(x => x.First().LastName).ToList();          
            var jsonResult = Json(autocompleteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

            //return "{ label: item.DataTerm, value: item.DataTerm }";
        }
    }
}
