using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using PCS1.Models;
using PCS1.Repositories;

namespace PCS1
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            //Register the Repository in the Unity Container
            container.RegisterType<IRepository<tbl_User, int,string>, UserRepository>();
            container.RegisterType<ICalendarRepository<tbl_Patient, long>, CalendarRepository>();
            container.RegisterType<ISettings<tbl_Settings>, SettingsRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}