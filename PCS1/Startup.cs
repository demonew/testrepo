﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PCS1.Startup))]
namespace PCS1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
