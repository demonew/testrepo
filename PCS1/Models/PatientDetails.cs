﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCS1.Models
{
    public class PatientDetails
    {
        public string id { get; set; }
        public string title { get; set; }
        public string date { get; set; }
        public string color { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public DateTime today { get; set; }
        public bool  allDay { get; set; }
      //  public List<int> dow { get; set; }    
        public List<ranges> ranges { get; set; }
    }
    public class ranges
    {
        public string  start { get; set; }
        public string  end { get; set; }
    }
}